import React from "react";
import { Scene, Router, Actions } from "react-native-router-flux";
import Login from "./components/Login";
import Students from "./components/Students";
import AddStudent from "./components/AddStudent";
const RouterComponent = () => {
  return (
    <Router>
      <Scene key="root" hideNavBar>
        <Scene key="auth">
          <Scene key="login" component={Login} title="Login" initial />
        </Scene>
        <Scene key="main">
          <Scene
            key="students"
            component={Students}
            title="Students"
            rightTitle="Add"
            onRight={() => Actions.addStudent()}
            initial
          />
          <Scene key="addStudent" component={AddStudent} title="Add Student" />
        </Scene>
      </Scene>
    </Router>
  );
};
export default RouterComponent;
