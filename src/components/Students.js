import React from "react";
import { View, Text, Button } from "react-native";
import { Actions } from "react-native-router-flux";

function Students(params) {
  return (
    <View>
      <Text>Students</Text>
      <Button
        onPress={() => {
          Actions.auth();
        }}
        title="Login"
      />
    </View>
  );
}

export default Students;
